<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

interface BuilderInterface
{
    public function addBasicInformation(): BuilderInterface;
}
