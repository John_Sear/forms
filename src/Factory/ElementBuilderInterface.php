<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementInterface;

interface ElementBuilderInterface extends BuilderInterface
{
    public function createElement(): ElementBuilderInterface;

    public function addElement(): ElementBuilderInterface;

    public function getElement(): ElementInterface;
}
