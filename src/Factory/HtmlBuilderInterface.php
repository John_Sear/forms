<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\ComponentInterface;

interface HtmlBuilderInterface
{
    public function setComponent(ComponentInterface $element): HtmlBuilderInterface;
}
