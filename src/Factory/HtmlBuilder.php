<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ButtonInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input\CheckboxInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input\HiddenInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\InputInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\SelectInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\TextAreaInterface;
use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Component\FieldSet\FieldInterface;
use JohnSear\Forms\Component\FieldSet\LegendInterface;
use JohnSear\Forms\Component\FieldSetInterface;
use JohnSear\Forms\Component\FormInterface;
use JohnSear\Forms\Component\WrapperInterface;
use JohnSear\Forms\Factory\Html\FieldSet\Field\Element\ButtonHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\Element\InputHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\Element\Input\CheckboxHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\Element\SelectHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\Element\TextAreaHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\HelpBlockHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\Field\LabelHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\FieldHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSet\LegendHtmlBuilder;
use JohnSear\Forms\Factory\Html\FieldSetHtmlBuilder;
use JohnSear\Forms\Factory\Html\FormHtmlBuilder;
use JohnSear\Forms\Factory\Html\WrapperHtmlBuilder;
use RuntimeException;

class HtmlBuilder implements HtmlBuilderInterface
{
    protected $component;
    protected $html;

    public function setComponent(ComponentInterface $element): HtmlBuilderInterface
    {
        $this->component = $element;

        return $this;
    }

    public function buildHtml(bool $minified = false): HtmlBuilderInterface
    {
        $html = '';

        $component = $this->component;

        if (! $component instanceof ComponentInterface) {
            throw new RuntimeException('No Component Interface given!');
        }

        if ($component instanceof ButtonInterface) {
            $buttonHtmlBuilder = (new ButtonHtmlBuilder());

            $html = $buttonHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof HiddenInterface) {
            $hiddenHtmlBuilder = (new InputHtmlBuilder());

            $html = $hiddenHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof TextAreaInterface) {
            $textAreaHtmlBuilder = (new TextAreaHtmlBuilder());

            $html = $textAreaHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof SelectInterface) {
            $selectHtmlBuilder = (new SelectHtmlBuilder());

            $html = $selectHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof InputInterface) {
            $inputHtmlBuilder = (new InputHtmlBuilder());

            $html = $inputHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof CheckboxInterface) {
            $inputHtmlBuilder = (new CheckboxHtmlBuilder());

            $html = $inputHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof HelpBlockInterface) {
            $helpBlockHtmlBuilder = (new HelpBlockHtmlBuilder());

            $html = $helpBlockHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof LabelInterface) {
            $labelHtmlBuilder = (new LabelHtmlBuilder());

            $html = $labelHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof FieldInterface) {
            $labelHtml = '';
            $label = $component->getLabel();
            if ($label instanceof LabelInterface) {
                $elements = $component->getElements();
                $firstElement = reset($elements);
                $label->setElementId($firstElement->getId());
                $labelHtml = (new self())->setComponent($label)->buildHtml($minified)->getHtml();
            }

            $elementsHtml = '';
            foreach ($component->getElements() as $element) {
                $elementsHtml .= (new self())->setComponent($element)->buildHtml($minified)->getHtml();
            }

            if ($component->getElementsWrapper() instanceof WrapperInterface) {
                $elementsHtml = (new WrapperHtmlBuilder())
                    ->setContent($elementsHtml)
                    ->buildHtml($component->getElementsWrapper());
            }

            $helpBlockHtml = '';
            if ($component->getHelpBlock() instanceof HelpBlockInterface) {
                $helpBlockHtml = (new self())->setComponent($component->getHelpBlock())->buildHtml($minified)->getHtml();
            }

            $fieldHtmlBuilder = (new FieldHtmlBuilder())->setLabelHtml($labelHtml)->setElementsHtml($elementsHtml)->setHelpBlockHtml($helpBlockHtml);

            $html = $fieldHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof LegendInterface) {
            $legendHtmlBuilder = (new LegendHtmlBuilder());

            $html = $legendHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof FieldSetInterface) {
            $legendHtml = '';
            if ($component->getLegend() instanceof LegendInterface) {
                $legendHtml = (new self())->setComponent($component->getLegend())->buildHtml($minified)->getHtml();
            }

            $fieldsHtml = '';
            foreach ($component->getFields() as $field) {
                $fieldsHtml .= (new self())->setComponent($field)->buildHtml($minified)->getHtml();
            }

            $fieldSetHtmlBuilder = (new FieldSetHtmlBuilder())->setLegendHtml($legendHtml)->setFieldsHtml($fieldsHtml);

            $html = $fieldSetHtmlBuilder->buildHtml($component);
        }

        if ($component instanceof FormInterface) {
            $fieldSetsHtml = '';
            foreach ($component->getFieldSets() as $fieldSet) {
                $fieldSetsHtml .= (new self())->setComponent($fieldSet)->buildHtml($minified)->getHtml();
            }

            $legendHtmlBuilder = (new FormHtmlBuilder())->setFieldSetsHtml($fieldSetsHtml);

            $html = $legendHtmlBuilder->buildHtml($component);
        }

        if ($component->getWrapper() instanceof WrapperInterface) {
            $html = (new WrapperHtmlBuilder())->setContent($html)->buildHtml($component->getWrapper());
        }

        if ($minified) {
            $html = $this->minifyHtml($html);
        }

        $this->html = $html;

        return $this;
    }

    public function getHtml(): string
    {
        return (string) $this->html;
    }

    private function minifyHtml(string $html): string
    {
        // do minify Action

        $minifiedHtml = $html;

        return $minifiedHtml;
    }
}
