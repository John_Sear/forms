<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FieldSetInterface;

interface FieldsetBuilderInterface extends BuilderInterface
{
    public function createFieldset(): FieldsetBuilderInterface;
    public function addLegend(): FieldsetBuilderInterface;
    public function addFieldElements(): FieldsetBuilderInterface;
    public function getFieldset(): FieldSetInterface;
}
