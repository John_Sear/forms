<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FieldSet\FieldInterface;

interface FieldBuilderInterface extends BuilderInterface
{
    public function createField(): FieldBuilderInterface;

    public function addLabel(): FieldBuilderInterface;
    public function addElement(): FieldBuilderInterface;
    public function addHelpBlock(): FieldBuilderInterface;

    public function getField(): FieldInterface;
}
