<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FormInterface;

interface FormBuilderInterface extends BuilderInterface
{
    public function createForm(): FormBuilderInterface;
    public function addFieldsetElements(): FormBuilderInterface;
    public function getForm(): FormInterface;
}
