<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet;

class FieldSetHtmlBuilder extends AbstractHtmlBuilder
{
    private $fieldsHtml = '';
    private $legendHtml = '';

    public function getFieldsHtml(): string
    {
        return $this->fieldsHtml;
    }

    public function setFieldsHtml(string $fieldsHtml): self
    {
        $this->fieldsHtml = $fieldsHtml;

        return $this;
    }

    public function getLegendHtml(): string
    {
        return $this->legendHtml;
    }

    public function setLegendHtml(string $legendHtml): self
    {
        $this->legendHtml = $legendHtml;

        return $this;
    }

    public function buildHtml(ComponentInterface $component): string
    {
        /** @var FieldSet $fieldSet */
        $fieldSet = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::FIELDSET_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($fieldSet, $template);

        $html = $this->replacePlaceholder('{{legend}}', $this->getLegendHtml(), $html);
        $html = $this->replacePlaceholder('{{fields}}', $this->getFieldsHtml(), $html);

        return $html;
    }
}
