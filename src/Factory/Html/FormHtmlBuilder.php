<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FormInterface;

class FormHtmlBuilder extends AbstractHtmlBuilder
{
    private $fieldSetsHtml = '';

    public function setFieldSetsHtml(string $fieldSetsHtml): self
    {
        $this->fieldSetsHtml = $fieldSetsHtml;

        return $this;
    }

    public function getFieldSetsHtml(): string
    {
        return $this->fieldSetsHtml;
    }

    public function buildHtml(ComponentInterface $component): string
    {
        /** @var FormInterface $form */
        $form = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::FORM_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($form, $template);

        $html = $this->replacePlaceholder('{{action}}', $form->getAction(), $html);
        $html = $this->replacePlaceholder('{{method}}', $form->getMethod(), $html);

        $html = $this->replacePlaceholder('{{fieldSets}}', $this->getFieldSetsHtml(), $html);

        return $html;
    }
}
