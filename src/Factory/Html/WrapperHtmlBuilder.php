<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\WrapperInterface;

class WrapperHtmlBuilder extends AbstractHtmlBuilder
{
    private $content = '';

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function buildHtml(ComponentInterface $component): string
    {
        /** @var WrapperInterface $wrapper */
        $wrapper = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::WRAPPER_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($wrapper, $template);

        $html = $this->replacePlaceholder('{{tag}}', $wrapper->getTag(), $html);
        $html = $this->replacePlaceholder('{{content}}', $this->getContent(), $html);

        return $html;
    }
}
