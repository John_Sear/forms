<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field\Element;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ButtonInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class ButtonHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var ButtonInterface $button */
        $button = $component;
        $template = $this->getElementTemplate($button->getType());

        $html = $this->replaceAbstractElementPlaceholder($button, $template);

        $html = $this->replacePlaceholder('{{text}}', $button->getText(), $html);

        return $html;
    }
}
