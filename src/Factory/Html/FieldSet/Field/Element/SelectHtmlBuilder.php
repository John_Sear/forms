<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field\Element;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\SelectInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class SelectHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var SelectInterface $select */
        $select = $component;
        $template = $this->getElementTemplate($select->getType());
        $optionTemplate = $this->getElementTemplate('option');

        $html = $this->replaceAbstractElementPlaceholder($select, $template);

        $values = (is_array(json_decode($select->getValue(), true))) ? json_decode($select->getValue(), true) : [$select->getValue()];

        $optionsHtml = '';
        foreach ($select->getOptions() as $value => $text) {
            $optionHtml = $this->replacePlaceholder('{{value}}', (string)$value, $optionTemplate);

            $optionHtml = $this->replacePlaceholder('{{text}}', (string)$text, $optionHtml);

            $selectedAttribute = (in_array((string)$value, $values, true)) ? ' selected' : '';
            $optionHtml = $this->replacePlaceholder('{{selected}}', $selectedAttribute, $optionHtml);

            $disabledAttribute = ''; /** @TODO: How to set and get option "disabled" state... */
            $optionHtml = $this->replacePlaceholder('{{disabled}}', $disabledAttribute, $optionHtml);

            $optionsHtml .= $optionHtml;
        }

        $html = $this->replacePlaceholder('{{options_loop}}', $optionsHtml, $html);

        return $html;
    }
}
