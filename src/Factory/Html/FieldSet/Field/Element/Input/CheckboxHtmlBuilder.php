<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input\CheckboxInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class CheckboxHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var CheckboxInterface $element */
        $element = $component;
        $template = $this->getElementTemplate($element->getType());

        $checked = (bool)$element->getValue();
        $element->setValue('1');

        if ($checked) {
            $element->addAttribute([
                'checked' => 'checked'
            ]);
        }

        $html = $this->replaceAbstractElementPlaceholder($element, $template);

        $inputHtml = $this->buildInputHtml($element);
        $html = $this->replacePlaceholder('{{input}}', $inputHtml, $html);

        $labelTile = (($label = $element->getLabel()) instanceof LabelInterface) ? $label->getTitle() : '';
        $html = $this->replacePlaceholder('{{label_title}}', $labelTile, $html);

        return $html;
    }

    private function buildInputHtml(ComponentInterface $component): string
    {
        $inputType = ElementTypesInterface::TEXT_INPUT_ELEMENT_TYPE;
        $inputTemplate = $this->getElementTemplate($inputType);

        $inputHtml = $this->replaceAbstractElementPlaceholder($component, $inputTemplate);

        return str_replace('form-control', '', $inputHtml);
    }
}
