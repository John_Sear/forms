<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field\Element;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\TextAreaInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class TextAreaHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var TextAreaInterface $input */
        $input = $component;
        $template = $this->getElementTemplate($input->getType());

        return $this->replaceAbstractElementPlaceholder($input, $template);
    }
}
