<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class LabelHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var LabelInterface $label */
        $label = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::LABEL_FIELD_FIELDSET_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($label, $template);

        $html = $this->replacePlaceholder('{{title}}', $label->getTitle(), $html);
        $html = $this->replacePlaceholder('{{element_id}}', $label->getElementId() , $html);

        return $html;
    }
}
