<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet\Field;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class HelpBlockHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var HelpBlockInterface $helpBlock */
        $helpBlock = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::HELP_BLOCK_FIELD_FIELDSET_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($helpBlock, $template);

        $html = $this->replacePlaceholder('{{text}}', $helpBlock->getHelp(), $html);

        return $html;
    }
}
