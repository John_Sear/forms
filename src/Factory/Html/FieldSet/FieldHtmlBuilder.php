<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet\FieldInterface;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class FieldHtmlBuilder extends AbstractHtmlBuilder
{
    private $labelHtml = '';
    private $elementHtml = '';
    private $helpBlockHtml = '';

    public function getLabelHtml(): string
    {
        return $this->labelHtml;
    }

    public function setLabelHtml(string $labelHtml): self
    {
        $this->labelHtml = $labelHtml;

        return $this;
    }

    public function getElementHtml(): string
    {
        return $this->elementHtml;
    }

    public function setElementsHtml(string $elementHtml): self
    {
        $this->elementHtml = $elementHtml;

        return $this;
    }

    public function getHelpBlockHtml(): string
    {
        return $this->helpBlockHtml;
    }

    public function setHelpBlockHtml(string $helpBlockHtml): self
    {
        $this->helpBlockHtml = $helpBlockHtml;

        return $this;
    }

    public function buildHtml(ComponentInterface $component): string
    {
        /** @var FieldInterface $field */
        $field = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE);

        if ($field->isFormGroup()) {
            $field->addClass('form-group');
        }

        $html = $this->replaceAbstractComponentsPlaceholder($field, $template);

        $html = $this->replacePlaceholder('{{label}}', $this->getLabelHtml(), $html);
        $html = $this->replacePlaceholder('{{elements}}', $this->getElementHtml(), $html);
        $html = $this->replacePlaceholder('{{helpBlock}}', $this->getHelpBlockHtml(), $html);

        return $html;
    }
}
