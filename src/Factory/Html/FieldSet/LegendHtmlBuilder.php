<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html\FieldSet;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet;
use JohnSear\Forms\Factory\Html\AbstractHtmlBuilder;

class LegendHtmlBuilder extends AbstractHtmlBuilder
{
    public function buildHtml(ComponentInterface $component): string
    {
        /** @var FieldSet\Legend $legend */
        $legend = $component;
        $template = $this->getComponentTemplate(ComponentTypesInterface::LEGEND_FIELDSET_COMPONENT_TYPE);

        $html = $this->replaceAbstractComponentsPlaceholder($legend, $template);

        $html = $this->replacePlaceholder('{{title}}', $legend->getTitle(), $html);

        return $html;
    }
}
