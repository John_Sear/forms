<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\ComponentTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Formatter\StringFormatter;

abstract class AbstractHtmlBuilder
{
    private $htmlFormat = HtmlFormatInterface::BS3_HTML_FORMAT;
    private $ds = DIRECTORY_SEPARATOR;

    public function getHtmlFormat(): string
    {
        return $this->htmlFormat;
    }

    public function setHtmlFormat(string $htmlFormat): void
    {
        $this->htmlFormat = $htmlFormat;
    }

    public function getBaseTemplatePath(string $htmlFormat = null)
    {
        $htmlFormat = $htmlFormat ?? $this->getHtmlFormat();

        $baseTemplatesPath = str_replace(
                'Factory' . $this->ds . 'Html',
                '',
                __DIR__
            ) . $this->ds . 'Templates' . $this->ds . 'format_' . $htmlFormat;

        return str_replace($this->ds . $this->ds, $this->ds, $baseTemplatesPath);
    }

    private function getTemplate(string $templatePath): string
    {
        if (! file_exists($templatePath)) {
            return '';
        }

        return file_get_contents($templatePath);
    }

    public function getComponentTemplate(string $type = ComponentTypesInterface::FORM_COMPONENT_TYPE): string
    {
        $format = $this->getHtmlFormat();

        $templatePath = '';

        $templatesBasePath = $this->getBaseTemplatePath($format);

        if (
            $type === ComponentTypesInterface::FORM_COMPONENT_TYPE ||
            $type === ComponentTypesInterface::FIELDSET_COMPONENT_TYPE ||
            $type === ComponentTypesInterface::WRAPPER_COMPONENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds . $type . '.phtml';
        }

        if (
            $type === ComponentTypesInterface::LEGEND_FIELDSET_COMPONENT_TYPE ||
            $type === ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds . ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds . $type . '.phtml';
        }

        if (
            $type === ComponentTypesInterface::HELP_BLOCK_FIELD_FIELDSET_COMPONENT_TYPE ||
            $type === ComponentTypesInterface::LABEL_FIELD_FIELDSET_COMPONENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds . ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds . ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds . $type . '.phtml';
        }

        return $this->getTemplate($templatePath);
    }

    public function getElementTemplate(string $type = ElementTypesInterface::TEXT_INPUT_ELEMENT_TYPE): string
    {
        $format = $this->getHtmlFormat();

        $templatePath = '';

        $templatesBasePath = $this->getBaseTemplatePath($format);

        if (
            $type === ElementTypesInterface::SELECT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::TEXTAREA_ELEMENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                $type . '.phtml';
        }

        if (
            $type === ElementTypesInterface::SELECT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::TEXTAREA_ELEMENT_TYPE ||
            $type === ElementTypesInterface::STATIC_ELEMENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                $type . '.phtml';
        }
        if ($type === ElementTypesInterface::OPTION_SELECT_ELEMENT_TYPE) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ElementTypesInterface::SELECT_ELEMENT_TYPE . $this->ds .
                $type . '.phtml';
        }

        if (
            $type === ElementTypesInterface::BTN_ELEMENT_TYPE ||
            $type === ElementTypesInterface::SUBMIT_BTN_ELEMENT_TYPE ||
            $type === ElementTypesInterface::RESET_BTN_ELEMENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ElementTypesInterface::BTN_ELEMENT_TYPE . '.phtml';
        }

        if (
            $type === ElementTypesInterface::TEXT_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::DATE_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::DATETIME_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::TIME_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::EMAIL_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::HIDDEN_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::NUMBER_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::PASSWORD_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::URL_INPUT_ELEMENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ElementTypesInterface::INPUT_ELEMENT_TYPE . '.phtml';
        }

        if (
            $type === ElementTypesInterface::CHECKBOX_INPUT_ELEMENT_TYPE ||
            $type === ElementTypesInterface::RADIO_INPUT_ELEMENT_TYPE
        ) {
            $templatePath  = $templatesBasePath . $this->ds .
                ComponentTypesInterface::FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ComponentTypesInterface::ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE . $this->ds .
                ElementTypesInterface::INPUT_ELEMENT_TYPE . $this->ds .
                $type . '.phtml';
        }

        return $this->getTemplate($templatePath);
    }

    protected function replacePlaceholder(string $placeholder, string $value, string $templateHtml): string
    {
        return str_replace($placeholder, $value, $templateHtml);
    }

    protected function replaceAbstractComponentsPlaceholder(ComponentInterface $component, string $templateHtml): string
    {
        $idAttribute = ($component->getId() !== '') ? ' id="' . $component->getId() . '"' : '';
        $templateHtml = $this->replacePlaceholder('{{idAttribute}}', $idAttribute, $templateHtml);

        $name = $component->getName();
        $templateHtml = $this->replacePlaceholder('{{name}}', StringFormatter::clean($name), $templateHtml);

        $classes = (count($component->getClasses())) ? ' ' . implode(' ', $component->getClasses()) : '';
        $templateHtml = $this->replacePlaceholder('{{classes}}', $classes, $templateHtml);

        $classAttribute = ($classes !== '') ? ' class="' . trim($classes) . '"' : '';
        $templateHtml = $this->replacePlaceholder('{{classAttribute}}', $classAttribute, $templateHtml);

        $attributes = '';
        foreach ($component->getAttributes() as $attributeKey => $attributeValue) {
            $attributes .= ' ' . $attributeKey . '="' . $attributeValue . '"';
        }
        $templateHtml = $this->replacePlaceholder('{{attributes}}', $attributes, $templateHtml);

        return $templateHtml;
    }

    protected function replaceAbstractElementPlaceholder(ElementInterface $element, string $templateHtml): string
    {
        $templateHtml = $this->replaceAbstractComponentsPlaceholder($element, $templateHtml);

        $type = $element->getType();
        $templateHtml = $this->replacePlaceholder('{{type}}', $type, $templateHtml);

        $value = ($element->getValue() === '' && $element->getDefault() !== '') ? $element->getDefault() : $element->getValue();
        $templateHtml = $this->replacePlaceholder('{{value}}', $value, $templateHtml);

        $required = ($element->isRequired()) ? ' required' : '';
        $templateHtml = $this->replacePlaceholder('{{required}}', $required, $templateHtml);

        $disabled = ($element->isDisabled()) ? ' disabled' : '';
        $templateHtml = $this->replacePlaceholder('{{disabled}}', $disabled, $templateHtml);

        $readonly = ($element->isReadonly()) ? ' readonly' : '';
        $templateHtml = $this->replacePlaceholder('{{readonly}}', $readonly, $templateHtml);

        return $templateHtml;
    }

    abstract public function buildHtml(ComponentInterface $component): string;
}
