<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory\Html;

interface HtmlFormatInterface
{
    const PLAIN_HTML_FORMAT = 'plain';
    const BS3_HTML_FORMAT = 'bs3';
}
