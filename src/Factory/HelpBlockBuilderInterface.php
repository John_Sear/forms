<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;

interface HelpBlockBuilderInterface extends BuilderInterface
{
    public function createHelpBlock(): HelpBlockBuilderInterface;

    public function addHelp(): HelpBlockBuilderInterface;

    public function getHelpBlock(): HelpBlockInterface;
}
