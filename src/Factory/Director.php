<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementInterface;
use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Component\FieldSet\FieldInterface;
use JohnSear\Forms\Component\FieldSetInterface;
use JohnSear\Forms\Component\FormInterface;

class Director
{
    public function buildForm(FormBuilderInterface $formBuilder): FormInterface
    {
        $formBuilder->createForm();

        $formBuilder->addBasicInformation();
        $formBuilder->addFieldsetElements();

        return $formBuilder->getForm();
    }

    public function buildFieldset(FieldSetBuilderInterface $fieldSetBuilder): FieldSetInterface
    {
        $fieldSetBuilder->createFieldset();

        $fieldSetBuilder->addBasicInformation();

        $fieldSetBuilder->addLegend();
        $fieldSetBuilder->addFieldElements();

        return $fieldSetBuilder->getFieldset();
    }

    public function buildField(FieldBuilderInterface $fieldBuilder): FieldInterface
    {
        $fieldBuilder->createField();

        $fieldBuilder->addBasicInformation();

        $fieldBuilder->addLabel();
        $fieldBuilder->addElement();
        $fieldBuilder->addHelpBlock();

        return $fieldBuilder->getField();
    }

    public function buildLabel(LabelBuilderInterface $labelBuilder): LabelInterface
    {
        $labelBuilder->createLabel();

        $labelBuilder->addBasicInformation();

        return $labelBuilder->getLabel();
    }

    public function buildElement(ElementBuilderInterface $elementBuilder): ElementInterface
    {
        $elementBuilder->createElement();

        $elementBuilder->addBasicInformation();

        $elementBuilder->addElement();

        return $elementBuilder->getElement();
    }

    public function buildHelpBlock(HelpBlockBuilderInterface $helpBlockBuilder): HelpBlockInterface
    {
        $helpBlockBuilder->createHelpBlock();

        $helpBlockBuilder->addBasicInformation();

        $helpBlockBuilder->addHelp();

        return $helpBlockBuilder->getHelpBlock();
    }

    public function buildHtml(ComponentInterface $element): string
    {
        $htmlBuilder = new HtmlBuilder();

        $htmlBuilder->setComponent($element);

        $htmlBuilder->buildHtml();

        return $htmlBuilder->getHtml();
    }
}
