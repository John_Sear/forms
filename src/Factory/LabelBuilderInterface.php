<?php declare(strict_types=1);

namespace JohnSear\Forms\Factory;

use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;

interface LabelBuilderInterface extends BuilderInterface
{
    public function createLabel(): LabelBuilderInterface;

    public function getLabel(): LabelInterface;
}
