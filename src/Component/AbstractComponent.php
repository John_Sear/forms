<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Component\Attribute\AttributesTrait;
use JohnSear\Forms\Component\Attribute\ClassesTrait;
use JohnSear\Forms\Formatter\StringFormatter;

abstract class AbstractComponent implements ComponentInterface
{
    protected $id;
    protected $name;
    protected $wrapper;
    protected $htmlFormat;

    use ClassesTrait;
    use AttributesTrait;

    public function setId(string $id): ComponentInterface
    {
        $this->id = StringFormatter::clean($id);

        return $this;
    }

    public function getId(): string
    {
        if ((string) $this->id === '') {
            return $this->getName();
        }

        return (string) $this->id;
    }

    public function setName(string $name): ComponentInterface
    {
        $this->name = StringFormatter::clean($name);

        return $this;
    }

    public function getName(): string
    {
        return (string) $this->name;
    }

    public function setWrapper(WrapperInterface $wrapper): ComponentInterface
    {
        $this->wrapper = $wrapper;

        return $this;
    }

    public function getWrapper(): ? WrapperInterface
    {
        return $this->wrapper;
    }

    public function setHtmlFormat(string $htmlFormat): ComponentInterface
    {
        $this->htmlFormat = trim($htmlFormat);
    }

    public function getHtmlFormat(): string
    {
        return (string) $this->htmlFormat;
    }
}
