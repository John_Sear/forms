<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet;

use JohnSear\Forms\Component\ComponentInterface;

interface LegendInterface extends ComponentInterface
{
    public function setTitle(string $title): LegendInterface;
    public function getTitle(): string;
}
