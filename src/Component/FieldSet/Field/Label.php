<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field;

use JohnSear\Forms\Component\AbstractComponent;

class Label extends AbstractComponent implements LabelInterface
{
    protected $title;
    protected $elementId;

    public function setTitle(string $title): LabelInterface
    {
        $this->title = trim($title);

        return $this;
    }

    public function getTitle(): string
    {
        return (string) $this->title;
    }

    public function setElementId(string $elementId): LabelInterface
    {
        $this->elementId = trim($elementId);

        return $this;
    }

    public function getElementId(): string
    {
        return (string) $this->elementId;
    }
}
