<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field;

use JohnSear\Forms\Component\ComponentInterface;

interface HelpBlockInterface extends ComponentInterface
{
    public function setHelp(string $help): HelpBlockInterface;
    public function getHelp(): String;
}
