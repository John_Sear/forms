<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field;

use JohnSear\Forms\Component\AbstractComponent;

class HelpBlock extends AbstractComponent implements HelpBlockInterface
{
    protected $help;

    public function setHelp(string $help): HelpBlockInterface
    {
        $this->help = trim($help);

        return $this;
    }

    public function getHelp(): String
    {
        return (string) $this->help;
    }
}
