<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field;

use JohnSear\Forms\Component\ComponentInterface;

interface LabelInterface extends ComponentInterface
{
    public function setTitle(string $title): LabelInterface;
    public function getTitle(): string;

    public function setElementId(string $elementId): LabelInterface;
    public function getElementId(): string;
}
