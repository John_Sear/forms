<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

class Input extends AbstractElement implements InputInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::TEXT_INPUT_ELEMENT_TYPE;
    }
}
