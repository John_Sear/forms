<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

use JohnSear\Forms\Component\AbstractComponent;
use JohnSear\Forms\Formatter\StringFormatter;

abstract class AbstractElement extends AbstractComponent implements ElementInterface
{
    protected $default;
    protected $value;
    protected $required = false;
    protected $disabled = false;
    protected $readonly = false;

    public function setDefault(string $default): ElementInterface
    {
        $this->default = StringFormatter::clean($default);

        return $this;
    }

    public function getDefault(): string
    {
        return (string) $this->default;
    }

    public function setValue(string $value): ElementInterface
    {
        $this->value = $value;

        return $this;
    }

    public function getValue(): string
    {
        $value = ( (string) $this->value !== '' ) ? (string) $this->value : $this->getDefault();

        return (string) $value;
    }

    public function setRequired(bool $required): ElementInterface
    {
        $this->required = $required;

        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setDisabled(bool $disabled): ElementInterface
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setReadonly(bool $readonly): ElementInterface
    {
        $this->readonly = $readonly;

        return $this;
    }

    public function isReadonly(): bool
    {
        return $this->readonly;
    }
}
