<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Number extends Input implements NumberInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::NUMBER_INPUT_ELEMENT_TYPE;
    }
}
