<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Password extends Input implements PasswordInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::PASSWORD_INPUT_ELEMENT_TYPE;
    }
}
