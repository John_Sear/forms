<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Url extends Input implements UrlInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::URL_INPUT_ELEMENT_TYPE;
    }
}
