<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Date extends Input implements DateInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::DATE_INPUT_ELEMENT_TYPE;
    }
}
