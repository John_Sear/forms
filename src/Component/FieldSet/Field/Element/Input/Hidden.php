<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Hidden extends Input implements HiddenInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::HIDDEN_INPUT_ELEMENT_TYPE;
    }
}
