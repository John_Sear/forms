<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Time extends Input implements TimeInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::TIME_INPUT_ELEMENT_TYPE;
    }
}
