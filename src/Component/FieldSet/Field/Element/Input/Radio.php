<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Radio extends Input implements RadioInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::RADIO_INPUT_ELEMENT_TYPE;
    }
}
