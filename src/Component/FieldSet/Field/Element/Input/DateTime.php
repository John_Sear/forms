<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class DateTime extends Input implements DateTimeInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::DATETIME_INPUT_ELEMENT_TYPE;
    }
}
