<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;

class Checkbox extends Input implements CheckboxInterface
{
    protected $asSwitch;
    /** @var null|LabelInterface */
    protected $label;

    public function getType(): string
    {
        return ElementTypesInterface::CHECKBOX_INPUT_ELEMENT_TYPE;
    }

    public function setAsSwitch(bool $asSwitch): Input
    {
        $this->asSwitch = $asSwitch;

        return $this;
    }

    public function asSwitch(): bool
    {
        return (bool) $this->asSwitch;
    }

    public function setLabel(LabelInterface $label): Input
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel(): ?LabelInterface
    {
        return $this->label;
    }
    
    
}
