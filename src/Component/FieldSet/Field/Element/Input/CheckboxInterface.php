<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\Input;
use JohnSear\Forms\Component\FieldSet\Field\Element\InputInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;

interface CheckboxInterface extends InputInterface
{
    public function setAsSwitch(bool $asSwitch): Input;
    public function asSwitch(): bool;
    
    public function setLabel(LabelInterface $label): Input;
    public function getLabel(): ?LabelInterface;
}
