<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Input;

use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\Input;

class Email extends Input implements EmailInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::EMAIL_INPUT_ELEMENT_TYPE;
    }
}
