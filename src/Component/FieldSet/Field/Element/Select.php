<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

use JohnSear\Forms\Formatter\StringFormatter;

class Select extends AbstractElement implements SelectInterface
{
    protected $options = [];

    public function getType(): string
    {
        return ElementTypesInterface::SELECT_ELEMENT_TYPE;
    }

    public function setOptions(array $options): SelectInterface
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function addOption(array $option): SelectInterface
    {
        if (count($option)) {
            $options = $this->getOptions();

            $arrayKeys = array_keys($option);
            $key = reset($arrayKeys);

            if(is_string($option[$key])) {
                $value = StringFormatter::clean($option[$key]);

                $options[$key] = $value;

                $this->setOptions($options);
            }
        }

        return $this;
    }
}
