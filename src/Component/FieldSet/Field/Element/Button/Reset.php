<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Button;

use JohnSear\Forms\Component\FieldSet\Field\Element\Button;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;

class Reset extends Button implements ResetInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::RESET_BTN_ELEMENT_TYPE;
    }
}
