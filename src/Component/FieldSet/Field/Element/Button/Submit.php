<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Button;

use JohnSear\Forms\Component\FieldSet\Field\Element\Button;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementTypesInterface;

class Submit extends Button implements SubmitInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::SUBMIT_BTN_ELEMENT_TYPE;
    }
}
