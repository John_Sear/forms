<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element\Button;

use JohnSear\Forms\Component\FieldSet\Field\Element\ButtonInterface;

interface SubmitInterface extends ButtonInterface
{

}
