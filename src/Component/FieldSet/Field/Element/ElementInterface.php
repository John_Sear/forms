<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

use JohnSear\Forms\Component\ComponentInterface;

interface ElementInterface extends ComponentInterface
{
    public function getType(): string;

    public function setDefault(string $default): ElementInterface;
    public function getDefault(): string;

    public function setValue(string $value): ElementInterface;
    public function getValue(): string;

    public function setRequired(bool $required): ElementInterface;
    public function isRequired(): bool;

    public function setDisabled(bool $disabled): ElementInterface;
    public function isDisabled(): bool;

    public function setReadonly(bool $readonly): ElementInterface;
    public function isReadonly(): bool;
}
