<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

interface SelectInterface extends ElementInterface
{
    public function setOptions(array $options): SelectInterface;
    public function getOptions(): array;

    public function addOption(array $option): SelectInterface;
}
