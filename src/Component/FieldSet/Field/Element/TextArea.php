<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

class TextArea extends AbstractElement implements TextAreaInterface
{
    protected $useWysiwyg;

    public function getType(): string
    {
        return ElementTypesInterface::TEXTAREA_ELEMENT_TYPE;
    }

    public function setUseWysiwyg(bool $useWysiwyg): TextAreaInterface
    {
        $this->useWysiwyg = $useWysiwyg;

        return $this;
    }

    public function useWysiwyg(): bool
    {
        return (bool) $this->useWysiwyg;
    }
}
