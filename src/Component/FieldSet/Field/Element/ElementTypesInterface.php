<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

interface ElementTypesInterface
{
    public const SELECT_ELEMENT_TYPE     = 'select';
    public const TEXTAREA_ELEMENT_TYPE   = 'textarea';
    public const INPUT_ELEMENT_TYPE      = 'input';
    public const STATIC_ELEMENT_TYPE     = 'static';
    public const BTN_ELEMENT_TYPE        = 'button';

    public const SUBMIT_BTN_ELEMENT_TYPE = 'submit';
    public const RESET_BTN_ELEMENT_TYPE  = 'reset';

    public const OPTION_SELECT_ELEMENT_TYPE  = 'option';

    public const CHECKBOX_INPUT_ELEMENT_TYPE = 'checkbox';
    public const DATE_INPUT_ELEMENT_TYPE     = 'date';
    public const DATETIME_INPUT_ELEMENT_TYPE = 'datetime-local';
    public const TIME_INPUT_ELEMENT_TYPE     = 'time';
    public const EMAIL_INPUT_ELEMENT_TYPE    = 'email';
    public const HIDDEN_INPUT_ELEMENT_TYPE   = 'hidden';
    public const NUMBER_INPUT_ELEMENT_TYPE   = 'number';
    public const PASSWORD_INPUT_ELEMENT_TYPE = 'password';
    public const RADIO_INPUT_ELEMENT_TYPE    = 'radio';
    public const TEXT_INPUT_ELEMENT_TYPE     = 'text';
    public const URL_INPUT_ELEMENT_TYPE      = 'url';
}
