<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

interface ButtonInterface extends ElementInterface
{
    /**
     * @return string
     */
    public function getText(): string;

    /**
     * @param string $text
     * @return Button
     */
    public function setText(string $text): ButtonInterface;
}
