<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

class Button extends AbstractElement implements ButtonInterface
{
    /** @var string */
    private $text = '';

    public function getType(): string
    {
        return ElementTypesInterface::BTN_ELEMENT_TYPE;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return ButtonInterface
     */
    public function setText(string $text): ButtonInterface
    {
        $this->text = $text;
        return $this;
    }


}
