<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet\Field\Element;

class StaticParagraph extends AbstractElement implements InputInterface
{
    public function getType(): string
    {
        return ElementTypesInterface::STATIC_ELEMENT_TYPE;
    }
}
