<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet;

use JohnSear\Forms\Component\AbstractComponent;
use JohnSear\Forms\Formatter\StringFormatter;

class Legend extends AbstractComponent implements LegendInterface
{
    protected $title;

    public function setTitle(string $title): LegendInterface
    {
        $this->title = StringFormatter::clean($title);

        return $this;
    }

    public function getTitle(): string
    {
        return (string) $this->title;
    }
}
