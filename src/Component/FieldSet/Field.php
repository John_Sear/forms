<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet;

use JohnSear\Forms\Component\AbstractComponent;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementInterface;
use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Component\WrapperInterface;

class Field extends AbstractComponent implements FieldInterface
{
    /** @var LabelInterface */
    protected $label;
    /** @var ElementInterface[] */
    protected $elements = [];
    /** @var WrapperInterface */
    protected $elementsWrapper;
    /** @var HelpBlockInterface */
    protected $helpBlock;
    /** @var bool */
    protected $formGroup = true;

    public function setLabel(LabelInterface $label): FieldInterface
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel(): ? LabelInterface
    {
        return $this->label;
    }

    /**
     * @param ElementInterface[] $elements
     */
    public function setElements(array $elements): FieldInterface
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * @return ElementInterface[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    public function addElement(ElementInterface $element): FieldInterface
    {
        if (! in_array($element, $this->elements, true)) {
            $this->elements[] = $element;
        }

        return $this;
    }

    /**
     * @param WrapperInterface $elementsWrapper
     * @return Field
     */
    public function setElementsWrapper(WrapperInterface $elementsWrapper): FieldInterface
    {
        $this->elementsWrapper = $elementsWrapper;
        return $this;
    }

    /**
     * @return WrapperInterface
     */
    public function getElementsWrapper(): ? WrapperInterface
    {
        return $this->elementsWrapper;
    }

    public function setHelpBlock(HelpBlockInterface $helpBlock): FieldInterface
    {
        $this->helpBlock = $helpBlock;

        return $this;
    }

    public function getHelpBlock(): ? HelpBlockInterface
    {
        return $this->helpBlock;
    }

    /**
     * @param bool $formGroup
     * @return Field
     */
    public function setFormGroup(bool $formGroup): FieldInterface
    {
        $this->formGroup = $formGroup;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFormGroup(): bool
    {
        return $this->formGroup;
    }
}
