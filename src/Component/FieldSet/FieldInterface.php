<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\FieldSet;

use JohnSear\Forms\Component\ComponentInterface;
use JohnSear\Forms\Component\FieldSet\Field\Element\ElementInterface;
use JohnSear\Forms\Component\FieldSet\Field\HelpBlockInterface;
use JohnSear\Forms\Component\FieldSet\Field\LabelInterface;
use JohnSear\Forms\Component\WrapperInterface;

interface FieldInterface extends ComponentInterface
{
    public function setLabel(LabelInterface $label): FieldInterface;
    public function getLabel(): ? LabelInterface;

    public function setElements(array $elements): FieldInterface;
    public function getElements(): array;
    public function addElement(ElementInterface $element): FieldInterface;

    public function setElementsWrapper(WrapperInterface $elementsWrapper): FieldInterface;
    public function getElementsWrapper(): ? WrapperInterface;

    public function setHelpBlock(HelpBlockInterface $helpBlock): ? FieldInterface;
    public function getHelpBlock(): ? HelpBlockInterface;

    public function setFormGroup(bool $formGroup): FieldInterface;
    public function isFormGroup(): bool;
}
