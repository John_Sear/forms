<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Component\Attribute\AttributesTraitInterface;
use JohnSear\Forms\Component\Attribute\ClassesTraitInterface;

interface ComponentInterface extends ClassesTraitInterface, AttributesTraitInterface
{
    public function setId(string $id): ComponentInterface;
    public function getId(): string;

    public function setName(string $name): ComponentInterface;
    public function getName(): string;

    public function setWrapper(WrapperInterface $wrapper): ComponentInterface;
    public function getWrapper(): ? WrapperInterface;

    public function setHtmlFormat(string $htmlFormat): ComponentInterface;
    public function getHtmlFormat(): string;
}
