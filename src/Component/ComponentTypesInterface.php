<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

interface ComponentTypesInterface
{
    public const WRAPPER_COMPONENT_TYPE  = 'wrapper';
    public const FORM_COMPONENT_TYPE     = 'form';
    public const FIELDSET_COMPONENT_TYPE = 'fieldset';

    public const LEGEND_FIELDSET_COMPONENT_TYPE = 'legend';
    public const FIELD_FIELDSET_COMPONENT_TYPE  = 'field';

    public const LABEL_FIELD_FIELDSET_COMPONENT_TYPE      = 'label';
    public const ELEMENT_FIELD_FIELDSET_COMPONENT_TYPE    = 'element';
    public const HELP_BLOCK_FIELD_FIELDSET_COMPONENT_TYPE = 'help';
}
