<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Formatter\StringFormatter;

class Form extends AbstractComponent implements FormInterface
{
    protected $action;
    protected $method;
    protected $multipart;
    protected $fieldSets = [];
    protected $useFieldSetsAsTabs = false;

    public function setAction(string $action): FormInterface
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): string
    {
        return (string) $this->action;
    }

    public function setMethod(string $method): FormInterface
    {
        $this->method = StringFormatter::clean($method);

        return $this;
    }

    public function getMethod(): string
    {
        $method = (is_string($this->method)) ? $this->method : 'post';

        return (string) $method;
    }

    public function setUseMultipart(bool $useMultipart): FormInterface
    {
        $this->multipart = $useMultipart;

        return $this;
    }

    public function useMultipart(): bool
    {
        return (bool) $this->multipart;
    }

    public function setFieldSets(array $fieldSets): FormInterface
    {
        $this->fieldSets = $fieldSets;

        return $this;
    }

    public function getFieldSets(): array
    {
        return $this->fieldSets;
    }

    public function addFieldSet(FieldSetInterface $fieldSet): FormInterface
    {
        $fieldSets = $this->getFieldSets();

        if (! in_array($fieldSet, $fieldSets, true)) {
            $fieldSets[] = $fieldSet;

            $this->setFieldSets($fieldSets);
        }

        return $this;
    }

    public function setUseFieldSetsAsTabs(bool $useFieldSetsAsTabs): FormInterface
    {
        $this->useFieldSetsAsTabs = $useFieldSetsAsTabs;

        return $this;
    }

    public function useFieldSetsAsTabs(): bool
    {
        return $this->useFieldSetsAsTabs;
    }
}
