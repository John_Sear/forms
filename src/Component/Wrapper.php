<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Formatter\StringFormatter;

class Wrapper extends AbstractComponent implements WrapperInterface
{
    protected $tag;

    public function setTag(string $tag): WrapperInterface
    {
        $this->tag = StringFormatter::clean($tag);

        return $this;
    }

    public function getTag(): String
    {
        return (string) $this->tag;
    }
}
