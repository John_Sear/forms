<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Component\FieldSet\FieldInterface;
use JohnSear\Forms\Component\FieldSet\LegendInterface;

interface FieldSetInterface extends ComponentInterface
{
    public function setLegend(LegendInterface $legend): FieldSetInterface;
    public function getLegend(): ?LegendInterface;

    public function setFields(array $fields): FieldSetInterface;
    public function getFields(): array;

    public function addField(FieldInterface $field): FieldSetInterface;
}
