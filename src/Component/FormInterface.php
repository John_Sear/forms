<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

interface FormInterface extends ComponentInterface
{
    public function setAction(string $action): FormInterface;
    public function getAction(): string;

    public function setMethod(string $method): FormInterface;
    public function getMethod(): string;

    public function setUseMultipart(bool $useMultipart): FormInterface;
    public function useMultipart(): bool;

    public function setFieldSets(array $fieldSets): FormInterface;
    public function getFieldSets(): array;

    public function addFieldSet(FieldSetInterface $fieldSet): FormInterface;

    public function setUseFieldSetsAsTabs(bool $useFieldSetsAsTabs): FormInterface;
    public function useFieldSetsAsTabs(): bool;
}
