<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

interface WrapperInterface extends ComponentInterface
{
    public function setTag(string $tag): WrapperInterface;
    public function getTag(): String;
}
