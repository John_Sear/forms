<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\Attribute;

interface ClassesTraitInterface
{
    public function setClasses(array $classes): ClassesTraitInterface;
    public function getClasses(): array;

    public function addClass(string $class): ClassesTraitInterface;
}
