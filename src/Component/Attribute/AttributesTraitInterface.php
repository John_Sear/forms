<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\Attribute;

interface AttributesTraitInterface
{
    public function setAttributes(array $attributes): AttributesTraitInterface;
    public function getAttributes(): array;

    public function addAttribute(array $attribute): AttributesTraitInterface;
}
