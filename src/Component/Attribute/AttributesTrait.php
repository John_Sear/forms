<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\Attribute;

use JohnSear\Forms\Formatter\StringFormatter;

trait AttributesTrait
{
    protected $attributes = [];

    public function setAttributes(array $attributes): AttributesTraitInterface
    {
        $this->attributes = $attributes;

        /** @var AttributesTraitInterface $this */
        return $this;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function addAttribute(array $attribute): AttributesTraitInterface
    {
        if (count($attribute)) {
            $attributes = $this->getAttributes();

            $arrayKeys = array_keys($attribute);
            $key = reset($arrayKeys);

            if(is_string($attribute[$key])) {
                $value = StringFormatter::clean($attribute[$key]);

                $attributes[$key] = $value;

                $this->setAttributes($attributes);
            }
        }

        /** @var AttributesTraitInterface $this */
        return $this;
    }
}
