<?php declare(strict_types=1);

namespace JohnSear\Forms\Component\Attribute;

use JohnSear\Forms\Formatter\StringFormatter;

trait ClassesTrait
{
    protected $classes = [];

    public function setClasses(array $classes): ClassesTraitInterface
    {
        $this->classes = $classes;

        /** @var ClassesTraitInterface $this */
        return $this;
    }

    public function getClasses(): array
    {
        return $this->classes;
    }

    public function addClass(string $class): ClassesTraitInterface
    {
        $classes = $this->getClasses();

        $class = StringFormatter::clean($class);

        if (! in_array($class, $classes, true)) {
            $classes[] = $class;
        }

        $this->classes = $classes;

        /** @var ClassesTraitInterface $this */
        return $this;
    }
}
