<?php declare(strict_types=1);

namespace JohnSear\Forms\Component;

use JohnSear\Forms\Component\FieldSet\FieldInterface;
use JohnSear\Forms\Component\FieldSet\LegendInterface;

class FieldSet extends AbstractComponent implements FieldSetInterface
{
    protected $legend;
    protected $fields = [];

    public function setLegend(LegendInterface $legend): FieldSetInterface
    {
        $this->legend = $legend;

        return $this;
    }

    public function getLegend(): ?LegendInterface
    {
        return $this->legend;
    }

    public function setFields(array $fields): FieldSetInterface
    {
        $this->fields = $fields;

        return $this;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function addField(FieldInterface $field): FieldSetInterface
    {
        $fields = $this->getFields();

        if (!in_array($field, $fields, true)) {
            $fields[] = $field;
            $this->setFields($fields);
        }

        return $this;
    }
}
