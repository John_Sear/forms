<?php declare(strict_types=1);

namespace JohnSear\Forms\Formatter;

class StringFormatter
{
    public static function clean(string $string): string
    {
        $cleanString = strip_tags($string);
        $cleanString = str_replace('"', '', $cleanString);
        $cleanString = trim($cleanString);

        return $cleanString;
    }
}
