<?php declare(strict_types=1);

namespace JohnSear\Forms\UnitTest\Factory\Mock;

use JohnSear\Forms\Component\Form;
use JohnSear\Forms\Component\FormInterface;
use JohnSear\Forms\Factory\BuilderInterface;
use JohnSear\Forms\Factory\FormBuilderInterface;

class FormBuilderMock implements FormBuilderInterface
{
    /** @var FormInterface $form */
    private $form;

    public function createForm(): FormBuilderInterface
    {
        $this->form = new Form();

        return $this;
    }

    public function addBasicInformation(): BuilderInterface
    {
        $this->form
            ->setAction('')
            ->setMethod('get')
            ->setUseMultipart(false)
            ->setUseFieldSetsAsTabs(true)
        ;

        return $this;
    }

    public function addFieldsetElements(): FormBuilderInterface
    {
        // not necessary for Test

        return $this;
    }

    public function getForm(): FormInterface {
        return $this->form;
    }
}
