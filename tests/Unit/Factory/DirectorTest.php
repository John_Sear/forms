<?php declare(strict_types=1);

namespace JohnSear\Forms\UnitTest\Factory;

use JohnSear\Forms\Component\FormInterface;
use JohnSear\Forms\Factory\Director;
use JohnSear\Forms\UnitTest\Factory\Mock\FormBuilderMock;
use PHPUnit\Framework\TestCase;

class DirectorTest extends TestCase
{
    public function testCanBuildForm()
    {
        $formBuilder = new FormBuilderMock();
        $sut         = new Director();

        $builtForm = $sut->buildForm($formBuilder);

        $this->assertInstanceOf(FormInterface::class, $builtForm);
    }
}
